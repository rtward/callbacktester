package CallbackTester::Schema::Result::Tag;

use strict;
use warnings;
use 5.12.0;

use base qw/DBIx::Class::Core/;

__PACKAGE__->table('tags');

__PACKAGE__->add_columns(
    id => {
        data_type         => 'integer',
        is_nullable       => 0,
        is_auto_increment => 1,
    },
    name => {
        data_type         => 'text',
        is_nullable       => 0,
    },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many('requests' => 'CallbackTester::Schema::Result::Request');

1;
