package CallbackTester::Schema;

use strict;
use warnings;
use 5.12.0;

use base qw/DBIx::Class::Schema/;

our $VERSION = '1';

__PACKAGE__->load_namespaces;

1;
