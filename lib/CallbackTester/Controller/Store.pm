package CallbackTester::Controller::Store;
use Mojo::Base 'Mojolicious::Controller';

use DateTime;

sub store_request {
  my $self = shift;
  my $tag_name = $self->stash('tag');

  if( not defined $tag_name ) {
      die "You must define a tag name";
  }

  if( $tag_name eq '' ) {
      die "You must define a tag name";
  }

  my $tag = $self->db->resultset('Tag')->find_or_create({ name => $tag_name });

  $tag->requests->create({
        datetime => DateTime->now,
        source_ip => $self->req->env->{REMOTE_ADDR} || 'Unknown',
        source_host => $self->req->env->{REMOTE_HOST} || 'Unknown',
        referer => $self->req->env->{HTTP_REFERER} || 'Unknown',
        method => $self->req->method,
        headers => $self->req->headers->to_string,
        body => $self->req->body,
      });

  $self->render_text('');
}

1;
