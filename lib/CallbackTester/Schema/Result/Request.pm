package CallbackTester::Schema::Result::Request;

use strict;
use warnings;
use 5.12.0;

use base qw/DBIx::Class::Core/;

__PACKAGE__->table('requests');

__PACKAGE__->load_components('InflateColumn::DateTime');

__PACKAGE__->add_columns(
    id => {
        data_type         => 'integer',
        is_nullable       => 0,
        is_auto_increment => 1,
    },
    tag => {
        data_type         => 'integer',
        is_nullable       => 0,
        is_foreign_key    => 1,
    },
    datetime => {
        data_type         => 'datetime',
        is_nullable       => 0,
    },
    source_ip => {
        data_type         => 'text',
        is_nullable       => 0,
    },
    source_host => {
        data_type         => 'text',
        is_nullable       => 0,
    },
    referer => {
        data_type         => 'text',
        is_nullable       => 0,
    },
    method => {
        data_type         => 'text',
        is_nullable       => 0,
    },
    headers => {
        data_type         => 'text',
        is_nullable       => 0,
    },
    body => {
        data_type         => 'text',
        is_nullable       => 0,
    },
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to('tag' => 'CallbackTester::Schema::Result::Tag');

1;

