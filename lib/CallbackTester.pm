package CallbackTester;
use Mojo::Base 'Mojolicious';

use CallbackTester::Schema;

sub startup {
  my $self = shift;

  $self->plugin('PODRenderer');

  my $db = CallbackTester::Schema->connect(
      'dbi:SQLite:share/callbacktester-schema.db'
  );

  $self->helper(db => sub {
        return $db;
      });

  my $r = $self->routes;
  $r->namespaces(['CallbackTester::Controller']);
  $r->any('/:tag')->to('store#store_request');
  $r->get('/results/:tag')->to('results#page');

  $self->hook(before_dispatch => sub {
          my $self = shift;
      });
}

1;
