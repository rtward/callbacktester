package CallbackTester::Controller::Results;
use Mojo::Base 'Mojolicious::Controller';

# This action will render a template
sub page {
  my $self = shift;
  my $tag_name = $self->stash('tag');

  if( not defined $tag_name ) {
      die "You must define a tag name";
  }

  if( $tag_name eq '' ) {
      die "You must define a tag name";
  }

  my $tag = $self->db->resultset('Tag')->find_or_create({ name => $tag_name });
  my @requests = $tag->requests->all;
  $self->stash({ requests => \@requests });
}

1;
